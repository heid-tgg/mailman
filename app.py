import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import pandas as pd
import mailman.mail as mail
import mailman.entities as db

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
folders_to_look = ["INBOX", "[Google Mail]/Gesendet"]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

st = ["MessageId", "Title", "Participates", "Start_Date", "End_Date", "In-Reply-To"]
app.layout = html.Div(children=[
    html.H1(children='Mailman', style={"color": "red"}),

    dash_table.DataTable(
        id='table',
        columns=[{"name": col, "id": col} for col in st],
        data=[dict()]),
    html.H1(id="TextField", children="Hallo"),
    html.Button(id='submit-button', n_clicks=0, children='Submit'),
    html.Button(id='create-folder-button', n_clicks=0, children='createFolder')
])

@app.callback(Output("TextField", "children"),
              [Input('create-folder-button', 'n_clicks')])
def update(n_clicks):
    connection = mail.MailConnection("config/credentials.json")
    connection.server.create_folder("test_folder")
    return("created")

@app.callback(Output('table', 'data'),
              [Input('submit-button', 'n_clicks')])
def update_output(n_clicks):
    session = db.start_db()
    if n_clicks is not None:
        print("load data", n_clicks)
        all = []
        connection = mail.MailConnection("config/credentials.json")
        all += mail.read_folder_to_array(connection, "INBOX", st)
        all += mail.read_folder_to_array(connection, "[Google Mail]/Gesendet", st)
        mail.update_folder_to_db(connection, folders_to_look, session)
        mail.read_folder_to_db(connection, "INBOX", session)
        mail.read_folder_to_db(connection, "[Google Mail]/Gesendet", session)

        df = pd.concat(all, axis=0)
        df.sort_values(["Start_Date"], inplace=True, ascending=0)
        print(df)
        return df.to_dict("rows")




if __name__ == '__main__':
    app.run_server(debug=True)
