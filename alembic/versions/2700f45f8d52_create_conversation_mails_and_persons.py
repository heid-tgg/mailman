"""create conversation, mails and persons

Revision ID: 2700f45f8d52
Revises: 
Create Date: 2019-01-17 21:45:52.901222

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base


# revision identifiers, used by Alembic.
revision = '2700f45f8d52'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'person',
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("name", sa.String(250)),
        sa.Column("domain", sa.String(250))
    )

    op.create_table(
        'mail',
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("subject", sa.String(250)),
        sa.Column("msgid", sa.String(250))
    )

    op.create_table(
        'conversation',
        sa.Column("id", sa.Integer, primary_key=True),
        sa.Column("title", sa.String(250)),
    )

    op.create_table(
        "mail_to_conversation",
        sa.Column("mail.id", sa.Integer, sa.ForeignKey('mail.id'), nullable=False),
        sa.Column("conversation.id", sa.Integer, sa.ForeignKey('conversation.id'), nullable=False)
    )

    op.create_table(
        "person_to_conversation",
        sa.Column("person.id", sa.Integer, sa.ForeignKey('person.id'), nullable=False),
        sa.Column("conversation.id", sa.Integer, sa.ForeignKey('conversation.id'), nullable=False)
    )


def downgrade():
    op.drop_table('Person')
